ASM=nasm
ASMFLAGS=-f elf64
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o: lib.asm

main.o: main.asm lib.inc dict.inc

dict.o: dict.asm lib.inc

%: %.o
	ld -o $@ $+
	rm *.o

program: lib.o dict.o main.o