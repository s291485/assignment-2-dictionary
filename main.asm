%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUF_LEN 255

section .bss
buffer_length: resb BUF_LEN

section .rodata
error_long: db "The element has gone beyond the buffer", 0
error: db "Element not found",0
section .text
global _start

_start:
    mov rdi, buffer_length
    mov rsi, BUF_LEN
    call read_word                 ; считываем слова максимальная длина которого 255, иначе вывод ошибки по длине
    test rax,rax
    je .long
    mov rdi, rax
    mov rsi, name_identification
    call find_word                 ; поиск элемента в словаре, есле оно есть, то переход на .yes
    test rax,rax
    jne .yes
    mov rdi, error
    call print_error               ; вывод сообщения о ошибке
    jmp .exit

.yes:
    mov rdi, rax
    add rdi, 8
    call string_length             ; определние длины
   	add rdi, rax                   ; прибавляем длину указателя на другой элемент
   	inc rdi
    push rax
   	call print_string              ; вывод результата
    pop rax
   	jmp .exit

.long:
	mov rdi, error_long
	call print_error               ; вывод ошибки

.exit:
	xor rdi,rdi
	call exit
