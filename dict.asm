%include "lib.inc"
global find_word

section .text

find_word:
    push rbx
    push r11
    mov rbx, rdi
    mov r11, rsi
    .loop:                  ; цикл пока не пройден весь словарь или элемент списка равен элементу переданому на сравнение
        test r11,r11        ; проверка на конец словаря
        je .end
        mov rdi, rbx
        mov rsi, r11
        add rsi, 8          
        call string_equals  ; проверка на равенство элемента из списка и элемента переданого на сравнение
        cmp rax,1
        je .end
        mov r11, [r11]
        jmp .loop
    .end:
        mov rax, r11
        pop r11
        pop rbx
        ret
