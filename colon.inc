%define name_identification 0 
%macro colon 2                      ; макрос на 2 элемента
    %2: dq name_identification      ; запись указателя на предыдущий элемент
        db %1,0                     ; запись значение элемента
    %define name_identification %2  ; запись ключа для его использования в следующем элементе
%endmacro
